<?php
  /* Template name: Contact */
  get_header('shop');
  
  the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
  <!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
    <div class="contactHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/gold-rings.png');">

      <div class="contactHeroContent">
        <div class="contactHeroContentWrapper">
          <h1>Contact Us</h1>
          <h6>Reach Out</h6>
          <div class="contactNumbers">
            <ul>
              <li>310-374-5700 <span id="lighter">PDT</span></li>
              <li>310-374-5799 <span id="lighter">FAX</span></li>
            </ul>
          </div>
          <p>We welcome your questions and comments. Contact us at the phone numbers listed or submit your inquiry using the form provided below. One of our dedicated customer experience team members will respond to your request shortly.</p>
          <div class="contactForm">
            <iframe width="500px" height="700" frameBorder="0" src="https://forms.na1.netsuite.com/app/site/crm/externalcasepage.nl?compid=4245635&formid=6&h=AACffht_aCizkHliRhu835AU9MS6pu0QW7I%3D"></iframe>

            
          </div>

        </div>
      </div>
    </div>
  <!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
  
  get_footer();
?>