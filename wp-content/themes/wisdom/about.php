<?php
	/* Template name: About */
	get_header('shop');
	
	the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
		<div class="aboutHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/about-hero.jpg');">

			<div class="aboutHeroContent">
				<div class="aboutHeroContentWrapper">
					<h1><?php the_field('title'); ?></h1>
					<p><?php the_field('about_text'); ?></p>
				</div>
				<ul>
					<li>
 						<a href="/shop">
 							<button>Browse Our Styles</button>
 						</a>
 					</li>
 					<li class="contactBtn">
 						<a href="/contact">
 							<button>Contact Us</button>
 						</a>
 					</li>

 				</ul>
			</div>
		</div>

		<div class="lifestyleSection" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/more-knobs.jpg');">
			<div class="exploreInstagram">
				<div class="exploreInstagramWrapper">
					<div class="exploreInstagramHeader">
						<h5>Our Lifestyle</h5>
						<h2>Explore our Instagram</h2>
					</div>

					<div class="gridThree" id="mobile_hide" style="padding-top:30px;">
						<!-- SnapWidget -->
						<script src="https://snapwidget.com/js/snapwidget.js"></script>
						<iframe src="https://snapwidget.com/embed/295584" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
					</div>

					<div class="gridThree" id="mobile_show" style="padding-top:30px; display:none;">
<!-- SnapWidget -->
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/398352" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
					</div>
				</div>
			</div>
		</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>


<style type="text/css">
li.media-list-item.no-border.responsives-grid-item {
	width: 50% !important;
}

</style>
<?php
	
	get_footer();
?>