<?php
	$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

	if ($feat_image == '') {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if (is_tax()) {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if (is_singular()) {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
	
	if ($_GET['s'] != '') {
		$feat_image = get_bloginfo('template_url') . '/pics/bg-body-empty.jpg';	
	}
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Wisdom Stone</title>
<!-- fonts -->
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!-- end fonts -->

<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/reset.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/default.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/animate.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/owl.carousel.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/media.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/custom.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/assets/fonts/font.css">
<link rel="stylesheet" media="all" href="<?php echo get_bloginfo('template_url'); ?>/css/updates.css">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.5/css/lightgallery.css">
<link rel = "stylesheet" href = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<!-- Slick Slider -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src='http://code.jquery.com/jquery-migrate-1.2.1.min.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/modernizr.js'></script>
<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/hoverIntent.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/superfish.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/wow.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/owl.carousel.min.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/js/scripts.js'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5'></script>
<script src='<?php echo get_bloginfo('template_url'); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js'></script>
<script src="https://use.fontawesome.com/1e11153706.js"></script>
<script>
	jQuery(document).on("scroll", function(){
		if (jQuery(document).scrollTop() > 1){
			jQuery("header").addClass("shrink");
		}
		else {
			jQuery("header").removeClass("shrink");
		}
	}); 
</script>
<?php
	echo wp_head();

	if (get_the_ID() == '2') {
		$body_cls = ' class="home" ';
	}
        else {
		$body_cls = ' class="inner" ';
        }
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2097606-3', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body <?php body_class(); ?> style="background-image: url(<?php echo $feat_image; ?>)">
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *HEADER
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<header role="banner" id="header" class="wow fadeIn" data-wow-delay="0.3s">
	<div class="container">
		<h1 class="logo">
			<a href="<?php echo get_bloginfo('url'); ?>" title="Wisdom Stone Homepage">
				<img class="img-responsive" src="<?php echo get_bloginfo('template_url'); ?>/images/logo-new.png" alt="Wisdom Stone logo">
			</a>
		</h1>
		<nav id="site-menu" class="clearfix">
			<a id="icon-Menu" href="#"><span></span></a>
			<ul class="sf-menu">
			<?php
				$defaults = array(
					'menu'            => '4',
					'container'       => '',
					'items_wrap'      => '%3$s'
				);
				
				wp_nav_menu($defaults);
			?>			
			</ul>
		</nav>

		<input type="checkbox" id="menu-trigger"class="menu__trigger"/>
<div class="menu">
  <label for="menu-trigger" class="menu__btn"><img src="<?php echo get_bloginfo('template_url'); ?>/pics/share-icon.png"></label>
  <div class="menu__list">
<!--     <div class="menu__item">
      <label for="menu-trigger" class="menu__link"><i class="menu__icon fa fa-heart"></i></label><span class="menu__tip">Save to Favorites</span>
    </div> -->
    <div class="menu__item">
      <label for="menu-trigger" class="menu__link">
      	<a href="https://www.pinterest.com/WisdomStoneHW/" target="_blank">
      		<i class="menu__icon fa fa-pinterest"></i>
      	</a>
      </label><span class="menu__tip">Pinterest</span>
    </div>
    <div class="menu__item">
      <label for="menu-trigger" class="menu__link">
      	<a href="https://plus.google.com/106937988638772125904" target="_blank">
      		<i class="menu__icon fa fa-google-plus"></i>
      	</a>
      </label><span class="menu__tip">Google Plus</span>
    </div>
    <div class="menu__item">
      <label for="menu-trigger" class="menu__link"><a href="https://www.facebook.com/wisdomstonehw/" target="_blank"><i class="menu__icon fa fa-facebook"></i></a></label><span class="menu__tip">Facebook</span>
    </div>
    <div class="menu__item">
      <label for="menu-trigger" class="menu__link">
      	<a href="https://twitter.com/WisdomStoneHW?lang=en" target="blank">
      		<i class="menu__icon fa fa-twitter"></i>
      	</a>
      </label><span class="menu__tip">Twitter</span>
    </div>
      <div class="menu__item">
      <label for="menu-trigger" class="menu__link">
        <a href="https://www.instagram.com/wisdomstonehw/" target="blank">
          <img src="<?php echo get_bloginfo('template_url'); ?>/pics/ig-social.png" class="ig-social">
        </a>
      </label><span class="menu__tip">Instagram</span>
    </div>
<!--     <div class="menu__item">
      <label for="menu-trigger" class="menu__link"><i class="menu__icon fa fa-envelope"></i></label><span class="menu__tip">Send by Email</span>
    </div> -->
  </div>
</div>

		<div id="search-box" class="clearfix">


			<form action="<?php echo get_bloginfo('url'); ?>" id="search-form">
				<input type="text" placeholder="Keywords:" value="<?php echo $_GET['s']; ?>" name="s" required id="s" aria-labelledby="search-label">
				<a class="search search-btn" href="">
					<!-- <span>SEARCH</span> -->
				</a>
			</form>

		</div>
	</div>
      <div class="link_out">
      <a href="https://www.architecturalmailboxes.com/" title="Visit our Parent Company!" target="_blank">
        <img class="img-responsive" src="<?php echo get_bloginfo('template_url'); ?>/pics/arch-logo-wht.png" alt="Wisdom Stone logo">
      </a>
    </div>
</header>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end HEADER
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ --> 