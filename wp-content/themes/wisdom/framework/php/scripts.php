<?php
	add_action("admin_init", "dashboard_init");
	
	function dashboard_init() {
		wp_register_style('custom-css_cms', get_bloginfo('template_url') . '/framework/css/style.css');
		wp_enqueue_style('custom-css_cms');

		wp_enqueue_script('global_js' , get_bloginfo('template_url') . '/framework/js/init.js');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('jquery-ui-sortable');
	}
?>