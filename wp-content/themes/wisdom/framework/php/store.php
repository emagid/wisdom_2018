<?php
	add_action('admin_menu', 'store_add_custom_box');
	add_action('save_post', 'store_save_postdata');

	function store_add_custom_box() {
		$server_url = $GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'];
			
		if (function_exists('add_meta_box')) {
			add_meta_box('store_extra_options1','Address info', 'store_extra_options1', 'store', 'side', 'low');
			add_meta_box('store_extra_options2','Map', 'store_extra_options2', 'store', 'normal', 'high');
		}
	}
	
	function store_extra_options1($post_id) {
		global $wpdb;
		
		$store_address = get_post_meta($post_id->ID, 'store_address', true);
		$store_city  	= get_post_meta($post_id->ID, 'store_city', true);
		$store_zip 	 = get_post_meta($post_id->ID, 'store_zip', true);
		$store_state   = get_post_meta($post_id->ID, 'store_state', true);
		$store_cn      = get_post_meta($post_id->ID, 'store_cn', true);
		$store_phone   = get_post_meta($post_id->ID, 'store_phone', true);
		$store_fax     = get_post_meta($post_id->ID, 'store_fax', true);
		$store_email   = get_post_meta($post_id->ID, 'store_email', true);
		$store_url 	 = get_post_meta($post_id->ID, 'store_url', true);
		$store_logo    = get_post_meta($post_id->ID, 'store_logo', true);
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<table width="100%" border="0">
		<tr>
			<td>Address</td>
		</tr>
		<tr>
			<td><input type="text" name="store_address" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_address)); ?>"/></td>
		</tr>
		<tr>
			<td>City</td>
		</tr>
		<tr>
			<td><input type="text" name="store_city" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_city)); ?>"/></td>
		</tr>
		<tr>
			<td>Zip Code</td>
		</tr>
		<tr>
			<td><input type="text" name="store_zip" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_zip)); ?>"/></td>
		</tr>
		<tr>
			<td>State</td>
		</tr>
		<tr>
			<td><input type="text" name="store_state" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_state)); ?>"/></td>
		</tr>
		<tr>
			<td>Country</td>
		</tr>
		<tr>
			<td><input type="text" name="store_cn" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_cn)); ?>"/></td>
		</tr>
		<tr>
			<td>Phone</td>
		</tr>
		<tr>
			<td><input type="text" name="store_phone" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_phone)); ?>"/></td>
		</tr>
		<tr>
			<td>Fax</td>
		</tr>
		<tr>
			<td><input type="text" name="store_fax" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_fax)); ?>"/></td>
		</tr>
		<tr>
			<td>Email</td>
		</tr>
		<tr>
			<td><input type="text" name="store_email" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_email)); ?>"/></td>
		</tr>
		<tr>
			<td>Url</td>
		</tr>
		<tr>
			<td><input type="text" name="store_url" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_url)); ?>"/></td>
		</tr>
		<tr>
			<td colspan="2">Logo</td>
		</tr>
		<tr>
			<td colspan="2">
				<input name="store_logo" type="text" style="width: 100%" value="<?php echo $store_logo; ?>">
				<input type="button" class="button button-primary upload-image" value="Upload image">
			</td>
		</tr>
		</table>
<?php
	}
	
	function store_extra_options2($post_id) {
		global $wpdb;
		
		$store_lat = get_post_meta($post_id->ID, 'store_lat', true);
		$store_lnt = get_post_meta($post_id->ID, 'store_lnt', true);
		
		$store_lat_label = ($store_lat == '') ? '37.96497319617501' : $store_lat;
		$store_lnt_label = ($store_lnt == '') ? '23.751227685319463' : $store_lnt;
?>
		<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
		<strong>Full Address:&nbsp;&nbsp;</strong></label>
		<input type="text" style="width:400px;" name="address" id="address" value="" /> <input class="button-primary" type="button" value="Find on map" onclick="showAddress(document.getElementById('address').value)"/>
		<br /><br />
		<table width="100%" border="0">
		<tr>
			<td width="50%">Latitude</td>
			<td style="padding-right: 12px;">Longitude</td>
		</tr>
		<tr>
			<td><input type="text" id="store_lat" name="store_lat" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_lat)); ?>"/></td>
			<td style="padding-right: 12px;"><input type="text" id="store_lnt" name="store_lnt" style="width:100%" value="<?php echo htmlspecialchars(stripslashes($store_lnt)); ?>"/></td>
		</tr>
		</table>
		<div align="center" id="map" style="width: 100%; height: 400px; margin:0 auto;"></div>
		<script type="text/javascript">
			var geocoder;
			var map;
			var markersArray = [];
													
			function initialize() {
				geocoder = new google.maps.Geocoder();
														
				map = new google.maps.Map(
					document.getElementById('map'), {
						center: new google.maps.LatLng(<?php echo $store_lat_label; ?>, <?php echo $store_lnt_label; ?>),
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}
				);
													
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(<?php echo $store_lat_label; ?>, <?php echo $store_lnt_label; ?>),
					map: map,
					draggable: true
				});
														
				markersArray.push(marker);
															
				google.maps.event.addListener(marker, 'dragend', function(event){
					jQuery('#store_lat').val(event.latLng.lat());
					jQuery('#store_lnt').val(event.latLng.lng());
				});
			} 
													
			function clearOverlays() {
				if (markersArray) {
					for (i in markersArray) {
						markersArray[i].setMap(null);
					}
				}
			}
																										
			function showAddress() {
				var address = document.getElementById('address').value;
														
				geocoder.geocode( { 'address': address}, function(results, status) {
															
					if (status == google.maps.GeocoderStatus.OK) {
						clearOverlays();
																	
						var coords = results[0].geometry.location;
																	
						jQuery('#store_lat').val(coords.lat());
						jQuery('#store_lnt').val(coords.lng());
																	
						map.setCenter(results[0].geometry.location);
																	
						var marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location,
							draggable: true
						});
															
						google.maps.event.addListener(marker, 'dragend', function(event){
							jQuery('#store_lat').val(event.latLng.lat());
							jQuery('#store_lnt').val(event.latLng.lng());
						});
																	
						markersArray.push(marker);
					} 
					else {
						alert('Geocode was not successful for the following reason: ' + status);
					}
				});
			}
			
			jQuery(document).ready(function(e) {
				initialize()
			});
		</script>
<?php
	}
	
	function store_save_postdata($post_id) {
		if ( !wp_verify_nonce( $_POST['cms_product_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
		}
			
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
			
		if ($_POST['post_type']=='store') {
			/* STORE */
			$store_address = $_POST['store_address'];
			$store_city  	= $_POST['store_city'];
			$store_zip 	 = $_POST['store_zip'];
			$store_state   = $_POST['store_state'];
			$store_cn      = $_POST['store_cn'];
			$store_phone   = $_POST['store_phone'];
			$store_fax     = $_POST['store_fax'];
			$store_email   = $_POST['store_email'];
			$store_url 	 = $_POST['store_url'];
			$store_logo    = $_POST['store_logo'];

			if ($store_address != '') 
				update_post_meta($post_id, 'store_address', $store_address);
			else
				delete_post_meta($post_id, 'store_address');

			if ($store_city != '') 
				update_post_meta($post_id, 'store_city', $store_city);
			else
				delete_post_meta($post_id, 'store_city');

			if ($store_zip != '') 
				update_post_meta($post_id, 'store_zip', $store_zip);
			else
				delete_post_meta($post_id, 'store_zip');

			if ($store_state != '') 
				update_post_meta($post_id, 'store_state', $store_state);
			else
				delete_post_meta($post_id, 'store_state');

			if ($store_cn != '') 
				update_post_meta($post_id, 'store_cn', $store_cn);
			else
				delete_post_meta($post_id, 'store_cn');

			if ($store_phone != '') 
				update_post_meta($post_id, 'store_phone', $store_phone);
			else
				delete_post_meta($post_id, 'store_phone');

			if ($store_fax != '') 
				update_post_meta($post_id, 'store_fax', $store_fax);
			else
				delete_post_meta($post_id, 'store_fax');

			if ($store_email != '') 
				update_post_meta($post_id, 'store_email', $store_email);
			else
				delete_post_meta($post_id, 'store_email');

			if ($store_url != '') 
				update_post_meta($post_id, 'store_url', $store_url);
			else
				delete_post_meta($post_id, 'store_url');

			if ($store_logo != '') 
				update_post_meta($post_id, 'store_logo', $store_logo);
			else
				delete_post_meta($post_id, 'store_logo');
			/* END STORE */
			
			/* MAP */
			$store_lat = $_POST['store_lat'];
			$store_lnt = $_POST['store_lnt'];

			if ($store_lat != '') 
				update_post_meta($post_id, 'store_lat', $store_lat);
			else
				delete_post_meta($post_id, 'store_lat');

			if ($store_lnt != '') 
				update_post_meta($post_id, 'store_lnt', $store_lnt);
			else
				delete_post_meta($post_id, 'store_lnt');
			/* END MAP */
		}
	}
?>