<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<!--Stop sidebar filter from overlapping footer-->
<script type="text/javascript">

$(window).scroll(function () { 

// distance from top of footer to top of document
footertotop = ($('#foot').position().top);
// distance user has scrolled from top, adjusted to take in height of sidebar (570 pixels inc. padding)
scrolltop = $(document).scrollTop()+660;
// difference between the two
difference = scrolltop-footertotop;

// if user has scrolled further than footer,
// pull sidebar up using a negative margin

if (scrolltop > footertotop) {

$('#stickem').css('margin-top',  0-difference);
}

else  {
$('#stickem').css('margin-top', 0);
}


});
</script>


<div class="catHero" style="background-image:url('/wp-content/uploads/shop-default.jpg');">
	<div class="catHeroContent" >
		<h2>Browse</h2>
		<h1 class="default_shop_text" style="display:none;">All Products</h1>
		<h1><?php echo single_cat_title();?></h1>

		<p>						
			<?php
				if(is_active_sidebar('product-banner-text')){
					dynamic_sidebar('product-banner-text');
				}
			?>
		</p>
	</div>


</div>
</div>
<div class="sidebarFilter" id="sticker">

	<div class="sidebarFilterBorder" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/purple-rings.png');">
	</div>
	
	<div class="sidebarFilterWrapper">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Shop Filter") ) : ?>
		<?php endif;?>
	</div>
	
</div>

<div class="product_contain">

	<div class="sidebarFilter" id="stickem">

		<div class="sidebarFilterBorder" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/purple-rings.png');">
		</div>
		
		<div class="sidebarFilterWrapper">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Shop Filter") ) : ?>
			<?php endif;?>
		</div>
		
	</div>
    <a class="">
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>
        </a>
</div>
<?php get_footer( 'shop' ); ?>

	<script type="text/javascript">
		$("li#yith-woo-ajax-navigation-7 h4").click(function(){
			$("li#yith-woo-ajax-navigation-7 .yith-wcan-list li").toggle();
		});
		$("li#yith-woo-ajax-navigation-8 h4").click(function(){
			$("li#yith-woo-ajax-navigation-8 .yith-wcan-list li").toggle();
		});
		$("li#yith-woo-ajax-navigation-9 h4").click(function(){
			$("li#yith-woo-ajax-navigation-9 .yith-wcan-list li").toggle();
		});
		$("li#yith-woo-ajax-navigation-10 h4").click(function(){
			$("li#yith-woo-ajax-navigation-10 .yith-wcan-list li").toggle();
		});

	</script>

	<script>
  $(document).ready(function(){
  	$("#stickem").sticky({topSpacing:97});
  });
</script>




<!-- change background image when on knobs or category page -->
<script type="text/javascript">


  if ($('.catHeroContent h1:contains("Pulls")').length > 0) {
    $(".catHero").css('background-image', 'url(/wp-content/uploads/shop-pulls.jpg)');

 	} else if ($('.catHeroContent h1:contains("Knobs")').length > 0) {
 	$(".catHero").css('background-image', 'url(/wp-content/uploads/shop-knobs.jpg)');


 } else {
     $(".catHero").css('background-image', 'url(/wp-content/uploads/shop-default.jpg)');

 }

</script>


<!-- change background image when color filter chosen -->
<script type="text/javascript">


 if ($("#yith-woo-ajax-navigation-7 > ul > li").hasClass("chosen"))  {
   $("body").addClass("color_ready");

// 	} else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(2)").hasClass("chosen")) {
// 	$(".catHero").css('background-image', 'url(/wp-content/uploads/filter-oilrubbedbronze.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(3)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-polishedchrome.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(4)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-polishedgold.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(5)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-rosegold.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(6)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-satingold.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(7)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-satinnickel.jpg)');

//     } else if ($("#yith-woo-ajax-navigation-7 > ul > li:nth-child(8)").hasClass("chosen")) {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/filter-white.jpg)');


// } else {
//     $(".catHero").css('background-image', 'url(/wp-content/uploads/shop-default.jpg)');

}

</script>

<script type="text/javascript">
if ($("body").hasClass("post-type-archive")) {
      $("h1.default_shop_text").show();
}

</script>