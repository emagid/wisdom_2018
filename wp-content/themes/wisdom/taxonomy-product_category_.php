<?php
	get_header();
	
	$cat = get_query_var('product_category');
	$cat = get_term_by('slug', $cat, 'product_category');
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="sidebarFilter">

			<div class="sidebarFilterBorder" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/purple-rings.png');">
			</div>

			<div class="sidebarFilterWrapper">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Shop Filter") ) : ?>
				<?php endif;?>
			</div>

		</div>

		<div class="catHero" style="background-image:url('<?php echo get_bloginfo('template_url'); ?>/pics/cat-hero.jpg');">
	<div class="catHeroContent">
		<h2>Browse</h2>
		<h1><?php echo $cat->name; ?></h1>
		<p>We offer a wide variety of colors and accents, from the classic designs to modern details, guaranteed to match your tastes. Whether you’re looking for a traditional curbside installation or a contemporary locking mailbox with minimalistic features, we can help you find an elegant mailbox to suit your needs. Each collection featured on our site was hand-chosen in order to provide our customers with superior products that will never go out of style.</p>
	</div>


</div>
		<div class="container wow fadeIn main-text" data-wow-delay="0.4s"> 
			<div class="products-entries clearfix"> 
			<?php
				$args = array(
					'post_type' 	  => 'product',
					'posts_per_page' => -1,
					'tax_query'	  => array(
						array(
							'taxonomy' => 'product_category',
							'terms'	=> $cat->term_id
						)
					)
				);
				
				$products = new WP_Query($args);
				
				if ($products->have_posts()) {
					while ($products->have_posts()) {
						$products->the_post();
						
						$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
			?>
						<!-- --> 
						<div class="productBlock">
							<a class=" wow fadeIn" data-wow-delay="0.4s" href="<?php echo get_permalink(get_the_ID()) ;?>">
								<img class="img-responsive aligncenter" src="<?php echo $feat_image; ?>" width="296" height="296" alt=" ">
								<h3><?php the_title(); ?></h3>
								<p>View Details</p>
							</a> 
						</div>
						<!-- --> 
			<?php
					}
				}
				else {
					echo '<p>No products found.</p>';		
				}
				
				wp_reset_postdata();
			?>
			</div>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>