<?php
	/* Template name: Stores */
	
	get_header();
	
	the_post();
?>
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAxdE9bFuFdKjMiKodvQKQ5B-o55YrpSyk"></script>

	<section id="dealer-map" class="container-fluid">
		<div class="row">
			
			<div id="map_wrapper" class="col-xs-12 col-sm-8 col-sm-push-4">
				<div id="map_canvas" class="mapping"></div>
			</div>
			
			<div class="section retailerSection">
				<div class="store-locator">
					<div class="row retailerSectionWrapper">
						<h1>Where to Buy</h1>
						<form action="" method="post" class="retailerForm">
							<div class="form-group clearfix">
								<div class="col-3">
									<input type="text" class="form-control" id="address" name="address" placeholder="Search by Zip Code">
								</div>
								<div class="col-3">
									<div class="styled-select">
										<select class="form-control" name="radius" id="radius" >
											<option value="5">5 miles</option>
											<option value="10">10 miles</option>
											<option value="50" selected="selected">50 miles</option>
											<option value="100">100 miles</option>
										</select>
									</div><!--styled-select-->
		<!--
									<select name="radius" id="radius" class="form-control">
										<option value="5">5 miles</option>
										<option value="10">10 miles</option>
										<option value="50">50 miles</option>
										<option value="100">100 miles</option>
									</select>
		-->
								</div>

								<div class="col-8">
									<button class="btn btn-dark btn-block search-store" type="button">SEARCH</button>							
								</div>
								<p class="text-center">
									<a class="btn btn-default" href="/browse-online-retailers">Browse Online Retailers</a>
								</p>
							</div>
						</form>
						<div class="dealers-listing">
							<div class="row">
								<div class="col-xs-12 col-sm-10 col-sm-offset-1">
									<h3 class="dealer-head"><span id="num-stores" data-results="0"></span> Stores Found</h3>

									<div class="retailerResults">
									<ul></ul>
									<div>
									
								</div>
								
								
								
							</div>
						</div>
						
					</div>
				</div> <!-- .store-locator -->
			</div>
			
		</div>
	</section>
	
<?php		
	get_footer();
?>