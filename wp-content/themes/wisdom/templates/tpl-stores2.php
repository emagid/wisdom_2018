<?php
	/* Template name: Stores */
	
	get_header();
	
	the_post();
?>
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAxdE9bFuFdKjMiKodvQKQ5B-o55YrpSyk&sensor=true_OR_false"></script>
	<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section retailerSection">
		<div class=" wow fadeIn main-text store-locator" data-wow-delay="0.4s"> 
			<div class="row retailerSectionWrapper">
				<h1>Where to Buy</h1>
				<div class="row">
					<form action="" method="post" class="retailerForm">
						<div class="form-group clearfix">
							<div class="col-3">
								<input type="text" class="form-control" id="address" name="address" placeholder="Address/ZipCode:">
							</div>
							<div class="col-3">
								<select name="radius" id="radius" class="form-control custom-select">
									<option value="5">5 miles</option>
									<option value="10">10 miles</option>
									<option value="50">50 miles</option>
									<option value="100">100 miles</option>
								</select>
							</div>
							<div class="col-8">
								<button class="btn btn-default btn-dark btn-lg search-store" type="button">SEARCH</button>
							</div>
						</div>
					</form>
					<p class="text-center">
						<a class="btn btn-default" href="<?php echo get_permalink(804); ?>">Browse Online Retailers</a>
					</p>	

					<div class="retailerResults">
						<ul>
							<li>
								<h4>Crown City Hardware (Pasadena)</h4>
								<p>Website</p> <span><a href="">http://wwww.crowncityhardware.com</a></span>
								<p>Phone</p> <span><a href="">626.794.0234</a></span>
								<p>Address</p> <span>1047 N. Allen Avenue, CA, USA 91104</span>
								<a href=""><button>Directions</button></a>
							</li>
							<li>
								<h4>Hillcrest Construction (Yorba Linda)</h4>
								<p>Website</p> <span><a href="">http://wwww.hillcrestconstruction.com</a></span>
								<p>Phone</p> <span><a href="">714.777.1072</a></span>
								<p>Address</p> <span>4842 School Street, CA, USA 92886</span>
								<a href=""><button>Directions</button></a>
							</li>
							<li>
								<h4>Koontz Hardware (West Hollywood)</h4>
								<p>Website</p> <span><a href="">http://wwww.koontz.com</a></span>
								<p>Phone</p> <span><a href="">626.794.0234</a></span>
								<p>Address</p> <span>1047 N. Allen Avenue, CA, USA 91104</span>
								<a href=""><button>Directions</button></a>
							</li>
						</ul>
					</div>				
				</div>
		</div>  
        
		</div>
	</div>
    <div class="section mapSection">
    	<div  id="dealer-map">
            <div id="map_wrapper" >
                <div id="map_canvas" class="mapping"></div>
            </div>
        </div>	
    	<div class="container">
            <div class="row dealers-listing" style="display: none;">
                <div class="container">
                    <h3 class="dealer-head"><span id="num-stores"></span> Stores found</h3>
                    <ul></ul>
                </div>
            </div>
        </div>
    </div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php		
	get_footer();
?>