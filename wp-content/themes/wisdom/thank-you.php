<?php
/* Template name: Thank You */
	get_header();
	
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section searchSection">
		<div class="container wow fadeIn main-text searchHeader thanksContainer" data-wow-delay="0.4s"> 
			<h2>Thank You for Contacting Us!</h1>
			<img class="pullImg" src="<?php echo get_bloginfo('template_url'); ?>/pics/2-pulls.png">
			<div class=" clearfix"> 
				<p class="default">We will be in touch shortly with a response.</p>
			</div>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>