<?php
	get_header();
	
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section searchSection">
		<div class="container wow fadeIn main-text searchHeader" data-wow-delay="0.4s"> 
			<h2>Search Results</h1>
			<img class="pullImg" src="<?php echo get_bloginfo('template_url'); ?>/pics/2-pulls.png">
			<div class="products-entries clearfix"> 
			<?php
				$args = array(
					'post_type' 	  => 'product',
					'posts_per_page' => -1,
					's'	  		  => $_GET['s']
				);
				
				$products = new WP_Query($args);
				
				if ($products->have_posts()) {
					while ($products->have_posts()) {
						$products->the_post();
						
						$feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
			?>
						<!-- --> 
						<a class="prod-entry wow fadeIn" data-wow-delay="0.4s" href="<?php echo get_permalink(get_the_ID()) ;?>">
							<img class="img-responsive aligncenter" src="<?php echo $feat_image; ?>" width="296" height="296" alt=" ">
							<h3><?php the_title(); ?></h3>
						</a> 
						<!-- --> 
			<?php
					}
				}
				else {
					echo '<p class="results">No products found.</p>';		
				}
				
				wp_reset_postdata();
			?>
			</div>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>